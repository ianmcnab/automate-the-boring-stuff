def collatz(num):

    if num % 2 == 0:
        num = num // 2
    else:
        num = ((3 * num) + 1)

    print(num)
    return num


try:
    number = int(input("Please enter a number greater than 1: "))
    while number != 1:
        number = collatz(number)

except ValueError:
    print("You broke it!")
