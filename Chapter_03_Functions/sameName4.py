def spam():
    print(eggs)     # ERROR, cannot implicitly reference global variable, must be explicit with
                    # use of global keyword
    eggs = 'spam local'


eggs = 'global'
spam()
