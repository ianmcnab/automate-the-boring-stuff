def spam():
    global eggs         # reference global value in function rather than an internal local variable
    eggs = 'spam'       # Updates the value of the global which is retained outside of function scope


eggs = "global"
spam()
print(eggs)
