# five times for loop version
print('My name is')
for i in range(5):
    print('Jimmy Five Times (' + str(i) + ')')

# Sum 0 to 100
total = 0
for num in range(101):
    total = total + num
print(total)

# five times while loop version
print('My name is')
i = 0
while i < 5:
    print('Jimmy Five Times (' + str(i) + ')')
    i = i + 1

# sliced ranges
for i in range(12, 16):         # 12-15, implicit step 1
    print(i)

for i in range(0, 10, 2):       # 0-9, step 2
    print(i)

# negative ranges
for i in range(5, -1, -1):      # 5-0, step -1
    print(i)

# negative ranges
for i in range(5, -1, 1):      # generates nothing as step is positive so next value is 6 (beyond the range)
    print(i)
