
# Need to add the inputs to define the two variables to make the program work!
name = input("What is your name?")
age = int(input("What is  your age?"))

if name == "Alice":
    print('Hi, Alice.')
elif age < 12:
    print('You are not Alice, kiddo.')
else:
    print('You are neither Alice nor a little kid.')
