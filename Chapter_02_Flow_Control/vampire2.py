
# Need to add the inputs to define the two variables to make the program work!
name = input("What is your name?")
age = int(input("What is  your age?"))

if name == "Alice":
    print('Hi, Alice.')
elif age < 12:
    print('You are not Alice, kiddo.')
elif age > 100:
    print('You are not Alice, grannie.')        # logic bug as vampire line wot trigger
elif age > 2000:
    print('Unlike you, Alice is not an undead, immortal vampire.')

